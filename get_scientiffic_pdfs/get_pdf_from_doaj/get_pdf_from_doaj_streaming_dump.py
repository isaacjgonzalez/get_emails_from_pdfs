# Parse do dump en json "a man" mediante regex. Funciona pero a maior parte dos papers non están en pdf, son links nos que logo podes descargar


import re

terms = ["business","finance","accounting"]

num_papers = 0
pdf_urls = []


for i in range(1,29):
    with open(f"doaj_article_data_2019-10-31/article_batch_{i}.json",mode='r',encoding='utf8', newline='\n') as f:
        for line in f:
            if any(x in line for x in terms):
                num_papers += 1
                result = re.findall(r"\w+://\w+\.\w+\.\w+/?[\w\.\?=#]*\.pdf", line)
                if len(result) > 0:
                    pdf_urls += result


print(num_papers)
print(pdf_urls)
