get_emails_from_pdfs

Create a list of emails from a bunch of pdf files. It avoids duplicated emails. The result is saved in a text file called "emails_list.txt", one email per line, the script override the list.

Use:
1 - Copy pdf files to "pdf_files" folder
2 - Execute the script: python3 get_emails_from_pdfs.py
