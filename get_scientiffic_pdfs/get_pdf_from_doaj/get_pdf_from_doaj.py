import re
import urllib3
from bs4 import BeautifulSoup

import json
import requests


def get_pdfs_from_doaj(url_search_doaj):
    urls_pdf = set()
    response = http.request('GET', url_search_doaj)
    soup = BeautifulSoup(response.data,'lxml')
    for link in soup.find_all('a'):
        url = link.get('href')
        if url is not None and url.endswith('.pdf'):
            urls_pdf.add(url)
    return urls_pdf





def get_pdfs_from_doaj_api(query):
    page = 1
    pagesize = 100 # 100 is max pagesize in API
    urls = []
    params = (
        ('page', '1'),
        ('pageSize', '100'),
    )

    api_url = "https://doaj.org/api/v1/search/articles/"+query
    headers = {'Accept': 'application/json'}
    response = requests.get(api_url, headers=headers, params=params)

    response_json = response.json()
    
    return





############# main



if __name__ == '__main__':
    # urllib3 configs
    #urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    #user_agent = {'user-agent': 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'}
    #http = urllib3.PoolManager(10, headers=user_agent)


    urls_pdf = get_pdfs_from_doaj_api("finance perspectives")

    print(urls_pdf)
