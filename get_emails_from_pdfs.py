#!/usr/bin/env python

import io
import os
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage


############# pdf to text

def extract_text_from_pdf(pdf_path):
    resource_manager = PDFResourceManager()
    fake_file_handle = io.StringIO()
    converter = TextConverter(resource_manager, fake_file_handle)
    page_interpreter = PDFPageInterpreter(resource_manager, converter)
    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh, caching=True,check_extractable=True):
            page_interpreter.process_page(page)
        text = fake_file_handle.getvalue()
    # close open handles
    converter.close()
    fake_file_handle.close()

    if text:
        return text



############# email extraction

from optparse import OptionParser
import os.path
import re

regex = re.compile(("([a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
                    "{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                    "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))

def file_to_str(filename):
    """Returns the contents of filename as a string."""
    with open(filename) as f:
        return f.read().lower() # Case is lowered to prevent regex mismatches.

def get_emails(s):
    """Returns an iterator of matched emails found in string s."""
    # Removing lines that start with '//' because the regular expression
    # mistakenly matches patterns like 'http://foo@bar.com' as '//foo@bar.com'.
    return (email[0] for email in re.findall(regex, s) if not email[0].startswith('//'))


############# list pdf files in folder

import glob

def list_pdf_files(pdfs_foldername):
    return glob.glob(pdfs_foldername+"/*.pdf")


############# main



if __name__ == '__main__':


    pdfs_foldername = 'pdf_files'
    emails_list_filename = 'emails_list.txt'

    all_emails = set()

    # Check if folder exist and is not empty
    if not os.path.exists(pdfs_foldername):
        os.makedirs(pdfs_foldername)
    if len(os.listdir(pdfs_foldername) ) == 0:
        print("Please add the pdfs to extract emails in this folder: "+pdfs_foldername+". And run the script again.")
        exit()

    # List pdfs filenames
    pdf_filenames = list_pdf_files(pdfs_foldername)
    for pdf_filename in pdf_filenames:
        print(pdf_filename.ljust(60),end=" - ")
        # Convert a pdf to text
        try:
            text = extract_text_from_pdf(pdf_filename)
        except Exception as ex:
            continue
            print("pdf_to_text error: "+str(ex),end=",")
        # Get emails from the text
        try:
            emails = get_emails(text)
            emails = list(emails)
        except Exception as ex:
            continue
            print("email extraction: "+str(ex),end=",")

        # Print line and add to all_emails set
        print(str(len(emails))+" emails founded")
        all_emails.update(emails)

    # Save emails_list and print goodbye message
    with open(emails_list_filename, 'w') as f:
        for item in all_emails:
            f.write("%s\n" % item)
    print()
    print(str(len(all_emails))+" emails have saved into emails_list.txt")
