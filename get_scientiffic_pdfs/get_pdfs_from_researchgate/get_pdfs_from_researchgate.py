
import re
import urllib3
from bs4 import BeautifulSoup

import wget


def get_researchgate_pub_page_from_search(url_researchgate_search):
    urls_pub = set()
    response = http.request('GET', url_researchgate_search)
    print(response.status)
    soup = BeautifulSoup(response.data,'lxml')
    for link in soup.find_all('a'):
        url = link.get('href')
        if url is not None and url.startswith('publication/'):
            urls_pub.add("https://www.researchgate.net/" + url)
        elif url is not None and url.startswith('https://www.researchgate.net/publication/'):
            urls_pub.add(url)
    return urls_pub


def get_pdfs_from_researchgate_pub_page(url_researchgate_pub):
    #url_researchgate_pub = "https://www.researchgate.net/publication/24065759_Business_Perspectives_in_Southeast_Europe"
    urls_pdf = set()
    response = http.request('GET', url_researchgate_pub)
    soup = BeautifulSoup(response.data,'lxml')
    for link in soup.find_all('a'):
        url = link.get('href')
        print("url -> ",url)
        if url is not None and url.endswith('.pdf'):
            urls_pdf.add(url)
    return urls_pdf


def download(url,filename):
    folder = "../pdf_files/"
    wget.download(url,folder+filename)


############# main



if __name__ == '__main__':

    search_terms = ["business perspectives","finance perspectives","accounting perspectives"]

    #urllib3 configs
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    user_agent = {'user-agent': 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'}
    http = urllib3.PoolManager(10, headers=user_agent)


    for research_term in search_terms:
        page_start = 1
        page_end = 3

        for page in range(page_start,page_end):
            url_base = f"https://www.researchgate.net/search/publications?q={re.sub(' ','%2B',research_term)}&page={page}"
            print(url_base)
            url_pubs = get_researchgate_pub_page_from_search(url_base)
            for url_pub in url_pubs:
                print(url_pub)
                urls_pdf = get_pdfs_from_researchgate_pub_page(url_pub)
                print(urls_pdf)
                for url in urls_pdf:
                    print(url)
                    download(url,url[41:url.find('?')]+'.pdf')
